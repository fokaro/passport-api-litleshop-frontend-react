import Sidebar from "./Sidebar";
import Navbar from "./Navbar";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {useNavigate } from "react-router-dom";
import { getUser, logout } from "./Authenficate";

const TransctionsTable = () => {
    const [marchandise, setMarchandise] = useState([]);
    const [detailsMarchandisesetDeleteItem, setDetailsMarchandise] = useState("");
    const [deleteItem, setDeleteItem] = useState("");
    const [table, setTable] = useState("");
    const navigate = useNavigate();
    const [user, setUser] = useState("");
    const [name, setName] = useState("");
  const [unitPrice, setUnitPrice] = useState("");
  const [totalPrice, setTotalPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [quantitySold, setQuantitySold] = useState("");
  const [lastPrice, setLastPrice] = useState("");
  const [id, setId] = useState("");

  const logOut = ()=> {
    logout();
    navigate("/login");

}
  
    const changeName = (e) => {
        let Name = e.target.value;
        console.log(Name);
        setName(Name);
      };
    
      const changeUnitPrice = (e) => {
        let unitPrice = e.target.value;
        console.log(unitPrice);
        setUnitPrice(unitPrice);
      };
    
      
    
      const changeQuantitySold = (e) => {
        let quantitySold = e.target.value;
        console.log(quantitySold);
        setQuantitySold(quantitySold);
      };
  
   
    

       useEffect(() => {
        
        async function transaction() {
          // You can await here
          const data = await fetch(
            "http://localhost:8000/api/transaction"
          ).then((response) => response.json());
    
          console.log("nouveau tbale");
          console.log(data);
          setMarchandise(data);
        }
        transaction();
      }, []); 

   

      return (
        <>
          <body className="g-sidenav-show  bg-gray-200"
          >
            <Sidebar />
            <main className="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
              <Navbar />
              <div className="container-fluid py-4">
                <div className="row">
                  <div className="col-12">
                    <div className="card my-4">
                      <div className="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                        <div className="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                          <h1 className="text-white text-capitalize text-center ps-3">
                            Transactions Table
                           
                          </h1>
                        </div>
                        <div className="avatar avatar-xl position-relative">
                  <img src="../assets/img/bruce-mars.jpg" alt="profile_image" className="w-150 border-radius-lg shadow-sm"/>
                </div>
                <div className="h-100">
              <h5 className="mb-1">
              {user.name}
              </h5>
             
              {user.email}
              
              
              <p className="mb-0 font-weight-normal text-sm">
                CEO / Co-Founder
              </p>
            </div>
                      </div>
                      <div className="col-auto my-auto">
                
              </div>
    
                      <div className="card-body px-0 pb-2">
                        <div className="table-responsive p-0">
                          <table className="table align-items-center justify-content-center mb-0">
                            <thead>
                              <tr>
                                <th className="text-black text-capitalize ps-3">
                                  Id
                                </th>
                                <th className="text-black text-capitalize ps-3">
                                  Name
                                </th>
                                <th className="text-black text-capitalize ps-3">
                                  Quantity Sold
                                </th>
                                <th className="text-black text-capitalize ps-3">
                                  Create At
                                </th>
                                <th className="text-black text-capitalize ps-3">
                                  Update At
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              {marchandise?.map((item) => (
                                <tr key={item.id} class="table-primary">
                                  <td>{item.id}</td>
                                  <td>{item.marchandise_name}</td>
                                  <td className="text-center">{item.quantity}</td>
                                  <td className="text-center">{item.created_at}</td>
                                  <td className="text-center">{item.updated_at}</td>


                                                                                                <td>
                                  
                                  </td>
    
                                
    
                                
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <footer className="footer py-4  ">
                  <div className="container-fluid">
                    <div className="row align-items-center justify-content-lg-between">
                      <div className="col-lg-6 mb-lg-0 mb-4">
                        <div className="copyright text-center text-sm text-muted text-lg-start">
                          ©{" "}
                          <script>document.write(new Date().getFullYear())</script>,
                          made with <i className="fa fa-heart"></i> by
                          <a
                            href="https://www.creative-tim.com"
                            className="font-weight-bold"
                            target="_blank"
                          >
                            Creative Tim
                          </a>
                          for a better web.
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <ul className="nav nav-footer justify-content-center justify-content-lg-end">
                          <li className="nav-item">
                            <a
                              href="https://www.creative-tim.com"
                              className="nav-link text-muted"
                              target="_blank"
                            >
                              Creative Tim
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              href="https://www.creative-tim.com/presentation"
                              className="nav-link text-muted"
                              target="_blank"
                            >
                              About Us
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              href="https://www.creative-tim.com/blog"
                              className="nav-link text-muted"
                              target="_blank"
                            >
                              Blog
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              href="https://www.creative-tim.com/license"
                              className="nav-link pe-0 text-muted"
                              target="_blank"
                            >
                              License
                            </a>
                         
                          </li>
                        
                          <li className="nav-item">
                  <a className="nav-link mb-0 px-0 py-1 " data-bs-toggle="tab" href="javascript:;" role="tab" aria-selected="false">
                    <i className="material-icons text-lg position-relative">settings</i>
                    <button type="button" className="btn btn-outline-primary btn-sm mb-0" 
                    onClick={logOut}
                    >LogOut</button>

                  </a>
                </li>
                     
                        </ul>
                      </div>
                    </div>
                  </div>
                </footer>
              </div>
            </main>
            <div className="fixed-plugin">
            {/*   <a className="fixed-plugin-button text-dark position-fixed px-3 py-2">
                <i className="material-icons py-2">settings</i>
              </a> */}
              <div className="card shadow-lg">
                <div className="card-header pb-0 pt-3">
                  <div className="float-start">
                    <h5 className="mt-3 mb-0">Material UI Configurator</h5>
                    <p>See our dashboard options.</p>
                  </div>
                  <div className="float-end mt-4">
                    <button className="btn btn-link text-dark p-0 fixed-plugin-close-button">
                      <i className="material-icons">clear</i>
                    </button>
                  </div>
                </div>
                <hr className="horizontal dark my-1" />
                <div className="card-body pt-sm-3 pt-0">
                  <div>
                    <h6 className="mb-0">Sidebar Colors</h6>
                  </div>
                  <a
                    href="javascript:void(0)"
                    className="switch-trigger background-color"
                  >
                    <div className="badge-colors my-2 text-start">
                      <span
                        className="badge filter bg-gradient-primary active"
                        data-color="primary"
                        onClick="sidebarColor(this)"
                      ></span>
                      <span
                        className="badge filter bg-gradient-dark"
                        data-color="dark"
                        onClick="sidebarColor(this)"
                      ></span>
                      <span
                        className="badge filter bg-gradient-info"
                        data-color="info"
                        onClick="sidebarColor(this)"
                      ></span>
                      <span
                        className="badge filter bg-gradient-success"
                        data-color="success"
                        onClick="sidebarColor(this)"
                      ></span>
                      <span
                        className="badge filter bg-gradient-warning"
                        data-color="warning"
                        onClick="sidebarColor(this)"
                      ></span>
                      <span
                        className="badge filter bg-gradient-danger"
                        data-color="danger"
                        onClick="sidebarColor(this)"
                      ></span>
                    </div>
                  </a>
    
                  <div className="mt-3">
                    <h6 className="mb-0">Sidenav Type</h6>
                    <p className="text-sm">
                      Choose between 2 different sidenav types.
                    </p>
                  </div>
                  <div className="d-flex">
                    <button
                      className="btn bg-gradient-dark px-3 mb-2 active"
                      data-class="bg-gradient-dark"
                      onClick="sidebarType(this)"
                    >
                      Dark
                    </button>
                    <button
                      className="btn bg-gradient-dark px-3 mb-2 ms-2"
                      data-class="bg-transparent"
                      onClick="sidebarType(this)"
                    >
                      Transparent
                    </button>
                    <button
                      className="btn bg-gradient-dark px-3 mb-2 ms-2"
                      data-class="bg-white"
                      onClick="sidebarType(this)"
                    >
                      White
                    </button>
                  </div>
                  <p className="text-sm d-xl-none d-block mt-2">
                    You can change the sidenav type just on desktop view.
                  </p>
    
                  <div className="mt-3 d-flex">
                    <h6 className="mb-0">Navbar Fixed</h6>
                    <div className="form-check form-switch ps-0 ms-auto my-auto">
                      <input
                        className="form-check-input mt-1 ms-auto"
                        type="checkbox"
                        id="navbarFixed"
                        onClick="navbarFixed(this)"
                      />
                    </div>
                  </div>
                  <hr className="horizontal dark my-3" />
                  <div className="mt-2 d-flex">
                    <h6 className="mb-0">Light / Dark</h6>
                    <div className="form-check form-switch ps-0 ms-auto my-auto">
                      <input
                        className="form-check-input mt-1 ms-auto"
                        type="checkbox"
                        id="dark-version"
                        onClick="darkMode(this)"
                      />
                    </div>
                  </div>
                  <hr className="horizontal dark my-sm-4" />
                  <a className="btn btn-outline-dark w-100" href="">
                    View documentation
                  </a>
                  <div className="w-100 text-center">
                    <a
                      className="github-button"
                      href="https://github.com/creativetimofficial/material-dashboard"
                      data-icon="octicon-star"
                      data-size="large"
                      data-show-count="true"
                      aria-label="Star creativetimofficial/material-dashboard on GitHub"
                    >
                      Star
                    </a>
                    <h6 className="mt-3">Thank you for sharing!</h6>
                    <a
                      href="https://twitter.com/intent/tweet?text=Check%20Material%20UI%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fsoft-ui-dashboard"
                      className="btn btn-dark mb-0 me-2"
                      target="_blank"
                    >
                      <i className="fab fa-twitter me-1" aria-hidden="true"></i>{" "}
                      Tweet
                    </a>
                    <a
                      href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/material-dashboard"
                      className="btn btn-dark mb-0 me-2"
                      target="_blank"
                    >
                      <i
                        className="fab fa-facebook-square me-1"
                        aria-hidden="true"
                      ></i>{" "}
                      Share
                    </a>

               
                  </div>
                </div>
              </div>
            </div>
          </body>
        </>
      );

      /* 
      useEffect(() => {
        async function transaction() {
          let options = {
          method: "GET",
          headers: { "Content-type": "application/json;charset=utf-8" },
          
        };
        const valeur = await fetch(
          "http://localhost:8001/api/transaction",
          options
        ).then((response) => response.json());
       
          console.log("voici le nouvel objet");
          console.log(valeur);
        }

        transaction();
    }, []);  */
       
}
  
  
 

  export default TransctionsTable;