import React, { Component } from 'react';
import { BrowserRouter as Router,Routes, Route, Link } from 'react-router-dom';
import Home from './components/home';
import About from './components/about';
import Contact from './components/contact';
import './App.css';
import SignUp from './components/SignUp';
import Login from './components/Login';
import Profil from './components/Profil';
import MarchandiseTable from './components/MarchandiseTable';
import CreateMarchandise from './components/CreateMarchandise';
import DetailsMarchandise from './components/DetailsMarchandise';
import EditerMarchandise from './components/EditerMarchandise';
import Transactions from './components/Transactions';






class App extends Component {
render() {
	return (
	<Router>
		<div className="App">
			{/* <ul className="App-header">
			<li>
				<Link to="/">Home</Link>
			</li>
			<li>
				<Link to="/about">About Us</Link>
			</li>
			<li>
				<Link to="/contact">Contact Us</Link>
			</li>
			</ul> */}
		<Routes>
				<Route exact path='/home' element={< Home />}></Route>
				<Route exact path='/about' element={< About />}></Route>
				<Route exact path='/contact' element={< Contact />}></Route>
				<Route exact path='/' element={< SignUp />}></Route>
				<Route exact path='/login' element={< Login />}></Route>
				<Route exact path='/profil' element={< Profil />}></Route>
				<Route exact path='/CreateMarchandise' element={< CreateMarchandise />}></Route>

				<Route exact path='/MarchandiseTable' element={< MarchandiseTable />}></Route>
				<Route exact path='/DetailsMarchandise' element={< DetailsMarchandise />}></Route>
				<Route exact path='/EditerMarchandise' element={< EditerMarchandise />}></Route>
				<Route exact path='/transaction' element={< Transactions />}></Route>







		</Routes>
		</div>
	</Router>
);
}
}

export default App;
